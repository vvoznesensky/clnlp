#! /usr/bin/python3

import sys
import os
from time import time
import numpy as np 
import pyopencl as cl
from mmap import mmap, PROT_READ
sys.path.append('../src')
from mps import mps_to_binary, permutate

device = cl.get_platforms()[0].get_devices()[0]
info = cl.device_info
assert device.get_info(info.NAME) == "Intel(R) Gen9 HD Graphics NEO"
context = cl.Context([device])
queue = cl.CommandQueue(context, device)

def prepare(mps_stem):
    if not os.path.exists(mps_stem):
        mps_path = "../test/mps/" + mps_stem + ".mps"
        mps_to_binary(mps_path)
        permutate(mps_path)

def do_mps(mps_stem):
    balances, froms, objectives, tos, upbounds = [
        cl.Buffer(context,
            cl.mem_flags.READ_ONLY | cl.mem_flags.COPY_HOST_PTR,
            0, mmap(file.fileno(), 0, prot=PROT_READ))
        for file in [
            open(mps_stem + "/" + filename)
            for filename in ("balances", "froms", "objectives", "tos", "upbounds")]
        ]

    n = froms.size // 2
    m = balances.size // 4
    assert not balances.size % 4
    assert not froms.size % 2
    assert froms.size == tos.size
    assert tos.size * 2 == objectives.size
    assert objectives.size == upbounds.size
    y_piece = 16384 - 8

    x = np.ones(n, np.float32)
    xbuf = cl.Buffer(context, cl.mem_flags.READ_ONLY| cl.mem_flags.COPY_HOST_PTR,
            n * 4, x)
    y = np.zeros(m, np.float32)
    ybuf = cl.Buffer(context, cl.mem_flags.READ_WRITE| cl.mem_flags.COPY_HOST_PTR,
            m * 4, y)

    do_ax(8 * 7 * 28, y_piece, xbuf, ybuf, froms, tos)
    #do_aty(8 * 7 * 28, y_piece, xbuf, ybuf, froms, tos)
    do_aty(8 * 7 * 16, y_piece, xbuf, ybuf, froms, tos)
    #do_aty(8 * 7 * 32, y_piece, xbuf, ybuf, froms, tos)

def do_ax(local_size, y_piece, xbuf, ybuf, frs, tos):

    n = xbuf.size // 4
    m = ybuf.size // 4

    x_piece = 1000000

    program = cl.Program(context, open("../src/ax.c").read())
    program.build()
    kernel = cl.Kernel(program, "ax_global")

    y_pcs = (m + y_piece - 1) // y_piece
    x_piece_by_y = cl.Buffer(context, cl.mem_flags.READ_WRITE, y_pcs * 4)

    kernel.set_args(np.uint32(n), xbuf, np.uint32(x_piece), frs, tos,
            np.uint16(m), ybuf, np.uint16(y_piece), x_piece_by_y,
            cl.LocalMemory(y_piece * 4))

    global_size = local_size * device.get_info(info.MAX_COMPUTE_UNITS)

    cl.enqueue_fill_buffer(queue, xbuf, np.float32(1), 0, n * 4)

    times = 100
    start = time()
    for i in range(times):
        cl.enqueue_fill_buffer(queue, ybuf, np.float32(0), 0, m * 4)
        cl.enqueue_fill_buffer(queue, x_piece_by_y, np.uint32(0), 0, y_pcs * 4)
        cl.enqueue_nd_range_kernel(queue, kernel, (global_size,), (local_size,))
    queue.finish()
    print((time() - start)/times, " seconds per ax")

    x, e = cl.enqueue_map_buffer(queue, xbuf, cl.map_flags.READ, 0, n, np.float32)
    y, e = cl.enqueue_map_buffer(queue, ybuf, cl.map_flags.READ, 0, m, np.float32)
    fs, e = cl.enqueue_map_buffer(queue, frs, cl.map_flags.READ, 0, n, np.uint16)
    ts, e = cl.enqueue_map_buffer(queue, tos, cl.map_flags.READ, 0, n, np.uint16)
    print(y)
    fc = np.bincount(fs, minlength = m)
    tc = np.bincount(ts, minlength = m)
    ethalon = fc - tc
    print(ethalon)
    assert not (ethalon - y).any()

def do_aty(local_size, y_piece, xbuf, ybuf, frs, tos):
    n = xbuf.size // 4
    m = ybuf.size // 4
    #y, e = cl.enqueue_map_buffer(queue, ybuf, cl.map_flags.READ, 0, m, np.float32)
    queue.finish()
    y = np.ndarray(m, np.float32)
    np.copyto(y, np.cumsum(np.ones(m, np.float32)))
    yimg = cl.Image(context, cl.mem_flags.READ_WRITE, cl.ImageFormat(cl.channel_order.R, cl.channel_type.FLOAT), shape=(m,))
    #cl.enqueue_copy(queue, yimg, y, is_blocking=True, origin=(0,), region=(m,))
    x, e = cl.enqueue_map_buffer(queue, xbuf, cl.map_flags.READ, 0, n, np.float32)
    fs, e = cl.enqueue_map_buffer(queue, frs, cl.map_flags.READ, 0, n, np.uint16)
    ts, e = cl.enqueue_map_buffer(queue, tos, cl.map_flags.READ, 0, n, np.uint16)
    
    fts = np.stack((fs, ts), axis = -1)
    ftsbuf = cl.Buffer(context,
      cl.mem_flags.READ_ONLY | cl.mem_flags.COPY_HOST_PTR, 0, fts)

    program = cl.Program(context, open("../src/aty.c").read())
    program.build()
    kernel = cl.Kernel(program, "aty")
    kernel.set_args(np.uint32(m), yimg, ftsbuf, xbuf, np.uint32(n))
    global_size = local_size * device.get_info(info.MAX_COMPUTE_UNITS)
    times = 100
    start = time()
    for i in range(times):
        cl.enqueue_fill_buffer(queue, xbuf, np.float32(0), 0, m * 4)
        cl.enqueue_nd_range_kernel(queue, kernel, (global_size,), (local_size,))
    queue.finish()
    print((time() - start)/times, " seconds per aty")
    print(x, x[1552: 1568])
    ethalon = y[fs] - y[ts]
    print(ethalon)
    assert not (ethalon - x).any()

#for task in ["netlarge1", "netlarge6"]:
for task in ["netlarge6", "netlarge1"]:
#for task in ["netlarge1",]:
    prepare(task)
    do_mps(task)

