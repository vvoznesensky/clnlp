"Load network description from special mps file"

from itertools import count
from struct import pack
import os
import numpy as np
import pyopencl as cl
from mmap import mmap, PROT_READ

# Read data and store it into fake or real vectors
def _pass(readline, vectors):
    balances, objectives, froms, tos, upbounds = vectors
    assert readline() == "NAME\n"
    readline()
    assert readline() == "OBJSENSE\n"
    assert readline() == "    MIN\n"
    assert readline() == "ROWS\n"
    assert readline() == " N  obj\n"
    for rows in count():
        line = readline()
        if line == "COLUMNS\n":
            break
        equ, row_name = line.split()
        assert equ == "E"
        assert row_name == "c" + str(rows + 1)
    #print("Columns")
    for columns in count():
        line = readline()
        if line == "RHS\n":
            break
        variable, row_name, value = line.split()
        assert variable == "x" + str(columns + 1)
        assert row_name == "obj"
        objectives[columns] = float(value)
        #print("value=", value, " objectives[", columns, "]=", objectives[columns])
        variable, row_name, value = readline().split()
        assert variable == "x" + str(columns + 1)
        assert row_name.startswith("c")
        froms[columns] = int(row_name[1:]) - 1
        assert value == "1"
        variable, row_name, value = readline().split()
        assert variable == "x" + str(columns + 1)
        assert row_name.startswith("c")
        tos[columns] = int(row_name[1:]) - 1
        assert value == "-1"

    line = readline()
    prevrow = -1
    while line != "RANGES\n":
        rhs, row_name, value = line.split()
        assert rhs == "rhs"
        assert row_name.startswith("c")
        row = int(row_name[1:]) - 1
        assert prevrow < row
        assert row < rows
        balances[prevrow + 1: row] = 0.
        prevrow = row
        balances[row] = float(value)
        line =readline()
    balances[row + 1: rows] = 0.

    assert readline() == "BOUNDS\n"
    line = readline()
    prevcolumn = -1
    while line != "ENDATA\n":
        upr, bound, variable, value = line.split()
        assert upr == "UP"
        assert bound == "bound"
        assert variable.startswith("x")
        column = int(variable[1:]) - 1
        assert prevcolumn < column
        assert column < columns
        upbounds[prevcolumn + 1: column] = np.inf
        prevcolumn = column
        upbounds[column] = float(value)
        line = readline()
    upbounds[column + 1: columns] = np.inf

    return rows, columns

class FileSinkVector(object):
    def __init__(self, filename):
        self.file = open(filename, "wb")

    def __binarize(self, value):
        if type(value) is float:
            return pack("f", value)
        else:
            return pack("H", value)

    def __setitem__(self, key, value):
        if type(key) is slice:
            step = 1 if key.step == None else key.step
            value = self.__binarize(value) * ((key.stop - key.start) // step)
        else:
            value = self.__binarize(value)
        self.file.write(value)

def path_from_mps(mps_name):
    filename = os.path.basename(mps_name)
    assert filename.endswith(".mps")
    return filename[:-4]

def mps_to_binary(mps_name):
    readline = open(mps_name).readline
    filename = path_from_mps(mps_name)
    try:
        os.mkdir(filename)
    except FileExistsError:
        pass
    vectors = map(lambda name: FileSinkVector(filename + "/" + name),
            ["balances", "objectives", "froms", "tos", "upbounds"])
    _pass(readline, vectors)

class _FakeVector(object):
    def __setitem__(self, key, value):
        self.oldval = value

    def __getitem__(self, key):
        return self.oldval

class NetworkFromMps(object):
    "Network description gained from special MPS file."

    def __init__(self, queue, filename):
        # Dry run, to calculate vector sizes
        readline = open(filename).readline
        fakes = (_FakeVector(),) * 5
        self.rows, self.columns = _pass(readline, fakes)

        # Allocate buffers on device and read the data into them
        context = queue.get_info(cl.command_queue_info.CONTEXT)
        readline = open(filename).readline
        self.balances, event = cl.enqueue_map_buffer(
            queue, cl.Buffer(context, cl.mem_flags.READ_ONLY, self.rows * 4),
            cl.map_flags.WRITE, 0, (self.rows,), np.float32)
        self.objectives, event = cl.enqueue_map_buffer(
            queue, cl.Buffer(context, cl.mem_flags.READ_ONLY, self.columns * 4),
            cl.map_flags.WRITE, 0, (self.columns,), np.float32)
        self.froms, event = cl.enqueue_map_buffer(
            queue, cl.Buffer(context, cl.mem_flags.READ_ONLY, self.columns * 2),
            cl.map_flags.WRITE, 0, (self.columns,), np.uint16)
        self.tos, event = cl.enqueue_map_buffer(
            queue, cl.Buffer(context, cl.mem_flags.READ_ONLY, self.columns * 2),
            cl.map_flags.WRITE, 0, (self.columns,), np.uint16)
        self.upbounds, event = cl.enqueue_map_buffer(
            queue, cl.Buffer(context, cl.mem_flags.READ_ONLY, self.columns * 4),
            cl.map_flags.WRITE, 0, (self.columns,), np.float32)
        assert (self.rows, self.columns) == _pass(
            readline,
            (self.balances, self.objectives, self.froms, self.tos, self.upbounds))

# Shuffle froms, tos and objectives to avoid collisions in ax_local.
mps_names = ["/home/w/Downloads/netlarge6.mps",
        "/home/w/Downloads/netlarge1.mps",]
filetypes = [("froms", np.uint16), ("tos", np.uint16),
            ("objectives", np.float32)]

def filearray(fpath, t):
    f = open(fpath, "r")
    a = np.frombuffer(mmap(f.fileno(), 0, prot=PROT_READ), t)
    return f, a

def permutate(mps_name):
    path = path_from_mps(mps_name)
    froms, tos, objectives = [filearray(path + "/" + filename, typename)
            for filename, typename in filetypes]
    permutation = np.random.permutation(froms[1].size)
    tos[1][permutation].tofile(path + "/tos")
    froms[1][permutation].tofile(path + "/froms")
    objectives[1][permutation].tofile(path + "/objectives")


