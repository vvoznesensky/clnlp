"Implementation of LP matrix of network of vertices."

from os.path import dirname, abspath
import pyopencl as cl
from pyopencl import program_info

CONTEXT = getattr(program_info, "CONTEXT")

class Network(object):
    """Network of vertices with sources(+) and sinks(-)
    as well as directed arcs with costs and limitations
    """

    programSource = open(dirname(abspath(__file__)) + "/ax.cl").read()

    def prod(self, queue, x):
        program = cl.Program(
            queue.get_info(CONTEXT),
            self.programSource)
        program.build()

    def tprod(self, queue, y):
        program = cl.Program(
            queue.get_info(CONTEXT),
            self.programSource)
        program.build()

class NetworkFromMps(object):
    "Network description gained from special MPS file."

    def __init__(self, context, balances, froms, tos, limits):
        self.balances = balances
        self.froms = froms
        self.tos = tos
        self.limits = limits
