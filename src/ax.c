typedef union fi32 {
  float f;
  int i;
} fi32;

// Scatter vector x[i=0...n-1] of arc flows to vector y[j=0...m-1]
// of balances of vertices in positions froms[i] with x[i] and
// in positions tos[i] with -x[i]. start is a lower bound of y in sums,
// count is a size of sums in elements. sums and y are float in reality!
// count is a number of sums in local buffer to store y values.
// count*sizeof(float) and start*sizeof(float) should divide evenly
// by cache line size.
// This function needs exclusive access to y.
// Try as_float() and as_int() instead of fi32.

void ax_local(unsigned n, const global float *x,
    const global unsigned short *froms, const global unsigned short *tos,
    unsigned short start, unsigned short count, volatile local fi32 *sums) {
  unsigned short finish = start + count;
  int prefetch_offset = get_local_id(0) - get_local_size(0);
  for(int i = get_local_id(0); i < n; i += get_local_size(0)) {
    prefetch(x + i + prefetch_offset, get_local_size(0));
    prefetch(froms + i + prefetch_offset, get_local_size(0));
    prefetch(tos + i + prefetch_offset, get_local_size(0));
    unsigned short from = froms[i];
    if(from >= start && from < finish) {
      unsigned short j = from - start;
      int old = sums[j].i;
      fi32 cmp;
      do {
        cmp.i = old;
        fi32 sum = {cmp.f + x[i]};
        old = atomic_cmpxchg(&((sums + j)->i), cmp.i, sum.i);
      } while(cmp.i != old);
    }
    unsigned short to = tos[i];
    if(to >= start && to < finish) {
      unsigned short j = to - start;
      int old = sums[j].i;
      fi32 cmp;
      do {
        cmp.i = old;
        fi32 sub = {cmp.f - x[i]};
        old = atomic_cmpxchg(&((sums + j)->i), cmp.i, sub.i);
      } while(cmp.i != old);
    }
  }
}

// Calculate y=a*x in global scale but all available computing units.
// m is a size of y, n is a size of x. m could be > local memory buffer.
// Split x into batches of x_piece size.
// Split y into pieces by size of local memory.
// Size of every piece of y is y_piece. No need to divide m by y_piece evenly.
// x_piece_by_y is a pointer to array of indices, so initialize it by zeros.
// Size of x_piece_by_pointer must be m / y_piece.
// sums is a local memory buffer of size y_piece.
// Number of groups * x_piece + (n - 1 + x_piece) / x_piece * x_piece
// must be < 2^32 - 1, otherwise x_piece_y_by_y would be overflown.
kernel void ax_global(const unsigned n, const global float *x,
    const unsigned x_piece, const global unsigned short *froms,
    const global unsigned short *tos, const unsigned short m,
    volatile global fi32 *y, const unsigned short y_piece,
    volatile global unsigned *x_piece_by_y, volatile local fi32 *sums) {
  const unsigned short y_pieces = (m + y_piece - 1) / y_piece;
  // Loop by y-pieces.
  for(unsigned j = 0; j < y_pieces; j++) {
    // y-piece sequential number for local group.
    unsigned short y_no = (get_group_id(0) + j) % y_pieces;
    // Get next x_piece of current y_piece.
    local unsigned x_no;
    if(!get_local_id(0))
      x_no = atomic_add(x_piece_by_y + y_no, x_piece);
    barrier(CLK_GLOBAL_MEM_FENCE);
    if(x_no >= n) continue;
    // Initialise sums by zero.
    for(int i = get_local_id(0); i < y_piece; i += get_local_size(0))
      sums[i].f = 0;
    // Iterate over x-pieces.
    barrier(CLK_LOCAL_MEM_FENCE);
    while(x_no < n) {
      // Spread x values of [y_no, y_no + pieces) indices over sums buffer.
      ax_local(min(x_piece, n - x_no), x + x_no, froms + x_no,
          tos + x_no, y_no * y_piece, y_piece, sums);
      // Get next x_piece of current y_piece.
      if(!get_local_id(0))
        x_no = atomic_add(x_piece_by_y + y_no, x_piece);
      barrier(CLK_GLOBAL_MEM_FENCE);
    }
    // Add sums to y atomically.
    for(unsigned j = get_local_id(0), idx = y_no * y_piece + get_local_id(0);
        j < y_piece && idx < m;
        j += get_local_size(0), idx += get_local_size(0)) {
      int old = y[idx].i;
      fi32 cmp;
      do {
        cmp.i = old;
        fi32 sum = {cmp.f + sums[j].f};
        old = atomic_cmpxchg(&((y + idx)->i), cmp.i, sum.i);
      } while(cmp.i != old);
    }
  }
}
