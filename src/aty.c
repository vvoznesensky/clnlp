typedef struct frto {
  ushort fr, to;
} frto_t;

static const sampler_t sampler = CLK_NORMALIZED_COORDS_FALSE |
  CLK_ADDRESS_NONE | CLK_FILTER_NEAREST;
// Scatter vector y[j=0..m-1] of vertice balances to vector x[i=0..n-1]
// of arc flows: x[i] = y[froms[i]] - y[tos[i]].
kernel void aty(ushort m, const read_only image1d_t y,
    const global frto_t *frtos, global float *x, uint count) {
  for(unsigned i = get_global_id(0); i < count; i += get_global_size(0)) {
    frto_t ft = frtos[i];
    float4 yfr = read_imagef(y, sampler, ft.fr);
    float4 yto = read_imagef(y, sampler, ft.to);
    x[i] = yfr[0] - yto[0];
  }
}
