"Test mps file loader"

from pyopencl import get_platforms, Context, CommandQueue
from mps import NetworkFromMps
DEVICE = get_platforms()[0].get_devices()[0]
CONTEXT = Context([DEVICE])
QUEUE = CommandQueue(CONTEXT, DEVICE)

def test_mps():
    "Test mps file loader"
    network = NetworkFromMps(QUEUE, "/home/w/Downloads/netlarge6.mps")
