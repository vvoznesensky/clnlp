"Some constants for common use."

import os.path as path
import numpy as np
import pyopencl as cl

#Each workgroup gonna occupy 1/2 of OpenCL computing unit.
WORKGROUP_SIZES = {
    "Intel(R) Gen9 HD Graphics NEO", 7 * 8 * 4}

#Workgroup local buffer size in bytes: local cache / 2 - some luft.
WORKGROUP_LOCAL = {
    "Intel(R) Gen9 HD Graphics NEO", 32000}

class cl_functor(object):
    def __init__(self, filename, funcname):
        self.

class multiplier(object):
    "Everything we need to make balances from flows and vice versa."
    def __init__(self, queue, froms, tos, m):
        self.queue = queue
        self.context = self.queue.get_info(cl.command_queue_info.CONTEXT)
        self.reconnect(froms, tos, m)
        dirname = path.dirname(__file__)
        self.axprog = cl.Program(open(dirname + "ax.cl").read())
        self.axprog.compile()
        self.axprog.build()
        #self.atxprog = cl.Program(open(dirname + "ax.cl").read())
        self.checkprog = cl.Program(open(dirname + "checkNodex.cl").read())
        self.checkprog.compile()
        self.checkprog.build()
        self.boolbuf = cl.Buffer()

    def reconnect(self, froms, tos, m):
        "Change network topology"
        assert (self.context == froms.get_info(cl.mem_info.CONTEXT))
        assert froms.get_info(cl.mem_info.TYPE) == cl.mem_object_type.BUFFER
        self.froms = froms
        assert self.checkprog.check(froms, )
        assert (self.context == tos.get_info(cl.mem_info.CONTEXT))
        assert (froms.get_info(cl.mem_info.SIZE) ==
                tos.get_info(cl.mem_info.SIZE))
        assert tos.get_info(cl.mem_info.TYPE) == cl.mem_object_type.BUFFER
        self.tos = tos
        # Do we need to check that each from and to is less than m?
        self.m = m

    def prod(self, x, y):
        "Make balances from flows."
        self.kernel.set_args(np.uint32(n), x, froms, tos, np.uint16(m), y,
                np.uint16(0), np.uint16(m), cl.LocalMemory(m * 4))
        cl.enqueue_nd_range_kernel(self.queue, self.kernel, (global_size,),
                (local_size,))

